# cunit
----

A small memory framework for running unit tests in the C language. There are three components to integrating this test suite into a project:

1. IO handling setup
2. Creating the test suites
3. Running the test suites

# IO handling setup
---

IO is done using two macros:

1. ```UNIT_PRINT()```
2. ```UNIT_FLUSH()```

These must be implemented to print messages and flush the message buffer. The default implementation uses ```printf()``` and ```fflush()```. However, for some machines this won't be practical and alternative printing mechanisms will have to be created.

# Creating the test suites
---

This testing framework contains test suites. The test suites contain test cases.
Test suites provide a way to catagorize test procedures. A test suite looks like this:

```C
#include <cunit.h>

TEST_SETUP()
{
    /* Code to any required setup before running a test.
     * This has a different scope than the tests, so local variable are
     * lost once this function returns.
     *
     * Even if the test suite doesn't require this functionality, the
     * setup function must still be defined. Even if it's an empty
     * function.
     */
}

TEST_TEARDOWN()
{
    /* Same as TEST_SETUP(), but it is called after running a test
     */
}

/********************************************************************************/
/* Test cases start here                                                        */
/********************************************************************************/
TEST( first_test )
{
    /* test code for the first test here.
     */
}

TEST( second_test )
{
    /* test code for the second test here.
     */
}

/* The arguments provided to the test function provide a means for identifying
 * the test when the test framework is run. For example, when "first_test" is
 * run, a message will be printed that says:
 *     TEST: first_test...
 * This argument is not string, ie, do not surround it with quotes.
 */

/********************************************************************************/
/* Test suite definition                                                        */
/********************************************************************************/
TEST_SUITE( first_test_suite )
{
    ADD_TEST( first_test );
    ADD_TEST( second_test );
}

/* The input to the TEST_SUITE macro has the same effect as the input to the
 * TEST() macros. It creates a name for the test suite. When the test framework
 * is run, a message will be printed that says:
 *     Running test suite: first_test_suite...
 *         Test: first_test...
 *         Test: second_test...
 * Of course, if there are failures in the test, there will more verbose printing
 * done.
 */
```

Within a TEST(), asserts are used to test conditions. If the assert fails, then the test framework counts that as a failure within the TEST(). The framework does not abort the TEST() though. It allows the TEST() to run to completion. A failure will not stop the testing framework from running (unless the failure corrupts the program). Assert statements looks like this:

```C
ASSERT( boolean_test_condition, "printf formatted message" );
```

The assert fails if the test condition is false. The message only gets printed if the assert fails. For example, the following assert and it's printed message look like:

```C
ASSERT( 1 > 3, "Wow, this should always fail, %d is always less than %d", 1, 3 );
```

```
FAILURE: source_code_line_number
Wow, this should always fail, 1 is always less than 3
```

A TEST() can be ended early using the ABORT_TEST() macro. It will print a message and return. Since the abort mechanism is only a return statement, it does not work within any function except the TEST(). For example:

```C
TEST( always_abort_test )
{
    ABORT_TEST("We should always abort");
}
```

```
Test: always_abort_test...
    ABORTED: We should always abort
```

# Running the test suites
---

The test suites will typically be run from main(). All test suites must be declared extern so the compiler doesn't fail.

```C
extern TEST_SUITE( first_test_suite );
extern TEST_SUITE( second_test_suite );

int main( int argc, char** argv )
{
    RUN_TEST_SUITE( first_test_suite );
    RUN_TEST_SUITE( second_test_suite );
    PRINT_DIAG()
}
```
