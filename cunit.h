/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 *
 * This test suite was inspired from the test suite at:
 * http://www.jera.com/techinfo/jtns/jtn002.html - January 2015
 */

#ifndef UNIT_H_
#define UNIT_H_

/*
 * ==========================================================================
 * ------------------------------ Printing ----------------------------------
 * ==========================================================================
 */
#include <stdio.h>
#define UNIT_PRINT( ... ) 	printf( __VA_ARGS__ )
#define UNIT_FLUSH( )		fflush(stdout)

/*
 * ==========================================================================
 * ------------------------------ Globals -----------------------------------
 * ==========================================================================
 */
extern unsigned int unit_asserts_passed, delta_unit_asserts_passed;
extern unsigned int unit_asserts_failed, delta_unit_asserts_failed;
extern unsigned int unit_tests_run;
extern unsigned int unit_tests_aborted;
extern unsigned int unit_asserts_failed_pre_test;
extern unsigned int unit_tests_passed, unit_tests_failed;

/*
 * ==========================================================================
 * ------------------------------ cunit core --------------------------------
 * ==========================================================================
 */
/* Test case function definition. In source:
 *     TEST( name_of_the_test )
 *     {
 *         *** test code here ***
 *     }
 */
#define TEST(name)			static void test_##name (void)

/* Test setup function definition. In source:
 *     TEST_SETUP( )
 *     {
 *         *** test suite specific setup ***
 *     }
 * This is always called before running any TEST(). 
 */
#define TEST_SETUP( )			static void _unit_test_setup( )

/* Same as TEST_SETUP(), but called after a TEST() is finished.
 */
#define TEST_TEARDOWN( )		static void _unit_test_teardown( )

/* Define a test suite within a source file.
 *     TEST_SUITE( name_of_test_suite )
 *     {
 *         ADD_TEST( name_of_first_test );
 *         ADD_TEST( name_of_second_test );
 *         ....
 *     }
 * The tests added to the suite must be defined in the same source
 * file using TEST( name_of_test ). The setup/teardown methods defined
 * in the source file will be called before and after each test case added.
 */
#define TEST_SUITE(suite) 		void all_tests_##suite (void)

/* Call within a TEST() to end it. It ends the test using a return
 * statement, so this won't work if called inside of a function that
 * was called by the TEST().
 */
#define ABORT_TEST(...)							\
	do {								\
		UNIT_PRINT( "\t\tABORTED: " );				\
		UNIT_PRINT(__VA_ARGS__ );				\
		UNIT_PRINT( "\n" );					\
		++unit_tests_aborted;					\
		++unit_tests_failed;					\
		return;							\
	} while( 0 )

/* Use these within a TEST() to assert conditions.
 * The first argument is a boolean expression. Evaluating to true
 * means the assert passes.
 * The following arguments after are printf style descriptions of the
 * assert. A string descriptor must be included, format specifiers are
 * optional. The string descriptor is only printed if the assert fails.
 * For example:
 *     ASSERT( 3 = (1+2), "Wow, how did this fail?" );
 *     ASSERT( x > y, "%d > %d", x, y );
 */
#define ASSERT(test, ...)						\
	do {								\
		if( !(test) ) { 					\
			UNIT_PRINT( "\t\tFAILURE: %d\n\t\t", __LINE__ );\
			UNIT_PRINT(__VA_ARGS__ );			\
			UNIT_PRINT( "\n" );				\
			UNIT_FLUSH( );					\
			++unit_asserts_failed;				\
		}							\
		else {							\
			++unit_asserts_passed;				\
		}							\
	} while(0)

/* Add a TEST() to the TEST_SUITE()
 */
#define ADD_TEST(name)							\
	do {								\
		UNIT_PRINT( "\tTest: %s...\n", #name );			\
		_unit_test_setup( );					\
		unit_asserts_failed_pre_test = unit_asserts_failed; 	\
		test_##name (); 					\
		_unit_test_teardown( );					\
		if( unit_asserts_failed > unit_asserts_failed_pre_test ) {	\
			unit_tests_failed += 1;					\
		}								\
		else {								\
			unit_tests_passed += 1;					\
		}								\
		++unit_tests_run;						\
	} while(0)

/* Will run all TEST() that were added to the TEST_SUITE().
 */
#define RUN_TEST_SUITE(suite)						\
	  do {								\
		UNIT_PRINT("Running test suite: %s...\n", #suite);	\
		delta_unit_asserts_passed = unit_asserts_passed;	\
		delta_unit_asserts_failed = unit_asserts_failed;	\
		all_tests_##suite ();					\
		UNIT_PRINT("Asserts passed: %d\nAsserts failed: %d\n\n",\
			unit_asserts_passed - delta_unit_asserts_passed,	\
			unit_asserts_failed - delta_unit_asserts_failed);	\
	} while(0)

/* Call after running every test suite to print the results of
 * testing.
 */
#define PRINT_DIAG()							\
	do {								\
		UNIT_PRINT("DIAGNOSTICS...\n");				\
		UNIT_PRINT("\tTests passed:\t%d\n"			\
			"\tTests failed:\t%d\n"				\
			"\tAsserts passed:\t%d\n"			\
			"\tAsserts failed:\t%d\n"			\
			"\tAsserts made:\t%d\n"				\
			"\tTest aborted:\t%d\n"				\
			"\tTests run:\t%d\n",				\
			unit_tests_passed,				\
			unit_tests_failed,				\
			unit_asserts_passed,				\
			unit_asserts_failed,				\
			unit_asserts_passed + unit_asserts_failed, 	\
			unit_tests_aborted,				\
			unit_tests_run );				\
	} while( 0 )


#endif /* UNIT_H_ */
